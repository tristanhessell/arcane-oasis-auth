'use strict';
/*eslint-env node, es6*/

require('dotenv').config({silent: true});
const express = require('express');
const bodyParser = require('body-parser');
const winston = require('winston');
const morgan = require('morgan');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'));
app.use('/', require('./routes/'));

app.listen(process.env.PORT, () => {
  winston.info(`Arcane Oasis Auth DB Service: Port ${process.env.PORT}`);
});

//need to export so you can test it with Super Test
module.exports = app;