'use strict';
/*eslint-env node, es6, mocha*/

require('dotenv').config();
const request = require('supertest');
const server = require('../index.js');

// const dbSetup = require('../util/sendCardsToMongo.js')('cardData', process.env.MONGODB_URI);

//TODO make the tests more descriptive in that they
// test structure of the returned content

describe('Auth Routing', () => {
//   before((done)=>{
//     // dbSetup.load().then(done);
//   });

//   after((done)=>{
// //    dbSetup.clean().then(done);
//   });

  it('should add a user', (done) => {
    const body = {
      username: 'addedUser',
      password: 'thirteen',
    };

    request(server)
      .post('/users')
      .send(body)
      .expect(201, done);
  });

  it('should 409 if you try to add a user that exists', (done) => {
    const body = {
      username: 'existingUser',
      password: 'thirteen',
    };

    request(server)
      .post('/users')
      .send(body)
      .expect(409, done);
  });


  it('should return for a valid user', (done) => {
    const body = {
      username: 'existingUser',
      password: 'thirteen',
    };

    request(server)
      .post('/users/authenticate')
      .send(body)
      .expect(204, done);
  });

  it('should 401 if an invalid user tries to authenticate', (done) => {
    const body = {
      username: 'existingUser',
      password: 'incorrectPassword',
    };

    request(server)
      .post('/users/authenticate')
      .send(body)
      .expect(401, done);
  });

  it('should 401 if an invalid user tries to authenticate', (done) => {
    const body = {
      username: 'incorrectUser',
      password: 'thirteen',
    };

    request(server)
      .post('/users/authenticate')
      .send(body)
      .expect(401, done);
  });

  it('should 401 if an invalid user tries to authenticate', (done) => {
    const body = {
      username: 'incorrectUser',
      password: 'thirteen',
    };

    request(server)
      .post('/users/authenticate')
      .send(body)
      .expect(401, done);
  });

  xit('should update an existing user', (done) => {
    const body = {
      username: 'existingUser',
      password: 'thirteen',
      newPassword: '13',
    };

    request(server)
      .put('/users/existingUser')
      .send(body)
      .expect(204, done);
  });

  it('should 404 if updating a non-existing user', (done) => {
    const body = {
      username: 'nonexistingUser',
      password: 'thirteen',
      newPassword: '13',
    };

    request(server)
      .put('/users/nonexistingUser')
      .send(body)
      .expect(404, done);
  });

  it('should 401 if updating a with an invalid password', (done) => {
    const body = {
      username: 'existingUser',
      password: 'invalidPassword',
      newPassword: '13',
    };

    request(server)
      .put('/users/existingUser')
      .send(body)
      .expect(404, done);
  });


  xit('should remove a user', (done) => {
    const body = {
      username: 'existingUser',
      password: 'thirteen',
    };

    request(server)
      .delete('/users/existingUser')
      .send(body)
      .expect(204, {
        list: `List 'legend of blue eyes' sucessfully deleted`,
      }, done);
  });

  xit('should 404 if removing a user that doesnt exist', (done) => {
    const body = {
      username: 'nonexistingUser',
      password: 'thirteen',
      newPassword: '13',
    };

    request(server)
      .delete('/users/nonexistingUser')
      .send(body)
      .expect(404, done);
  });

  xit('should 401 if removing a user with the incorrect password', (done) => {
    const body = {
      username: 'nonexistingUser',
      password: 'thirteen',
      newPassword: '13',
    };

    request(server)
      .delete('/users/nonexistingUser')
      .send(body)
      .expect(401, done);
  });
});


