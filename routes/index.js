'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router = express.Router();

router.use('/users', require('./auth.js'));

module.exports = router;
