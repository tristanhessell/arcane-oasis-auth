'use strict';
/*eslint-env node, es6*/

const express = require('express');
const router = express.Router();
const winston = require('winston');

const dbClient = require('../dbClient.js');

router.get('/:username', (req, res) => {
  const {username} = req.params;

  dbClient.checkUserExists(username)
    .then((found) => {
      if (found) {
        res.sendStatus(204);
      } else {
        res.sendStatus(404);
      }
    }).catch(() => {
      res.send('error for some reason');
    })
});

router.post('/', (req, res) => {
  const {username, password} = req.body;

  dbClient.addUser(username, password)
    .then(() => {
      res.sendStatus(201);
    })
    .catch((error) => {
      winston.error(error)
      res.status(409).send({
        error,
      });
    });
});

//200 or 404 if not found
router.put('/:username', (req, res) => {
  const {password, newPassword} = req.body;
  const {username} = req.params;

  dbClient.updateUser(username, password, newPassword)
    .then(() => {
      res.sendStatus(204);
    })
    .catch(() => {
      res.sendStatus(404);
    });
});


//204 if and the token if works
//401  if invalid credentials
router.post('/:username/authenticate', (req, res) => {
  const {username, password} = req.body;

  dbClient.checkUser(username, password)
    .then(() => {
      res.sendStatus(204);
    })
    .catch((error) => {
      winston.error(error)
      res.sendStatus(401);
    });
});

module.exports = router;
