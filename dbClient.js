'use strict';
/*eslint-env node, es6*/

const MongoClient = require('mongodb').MongoClient;
const url = process.env.MONGODB_URI;
const AUTH_COLLECTION_NAME = 'AUTH';

//index is on the username - so you
// wont be able to add two of the same users
const addUser = (username, password) =>
  MongoClient.connect(url)
    .then((db) => addOneUser(db, {username, password})
      .then((writeObj) => {
        db.close();

        if (writeObj.result.ok) {
          return `User '${username}' added`;
        }

        throw `Cannot add user: '${username}'`;
      })
    );

//whether its found or not, the value is .then'd
////which passes true or false
const checkUserExists = (username) =>
  MongoClient.connect(url)
    .then((db) => findUser(db, {username})
      .then((foundUser) => {
        db.close();
        console.log(foundUser);
        return foundUser !== null;
      }));


//find the user AND that password
const checkUser = (username, password) =>
  MongoClient.connect(url)
    .then((db) => findUser(db, {username, password})
      .then((foundUser) => {
        db.close();

        if (foundUser !== null) {
          return `User '${username}' verified`;
        }

        throw `Cannot validate user`;
      }));

const updateUser = (username, password, newPassword) =>
  MongoClient.connect(url)
    .then((db) =>
      replaceUser(db, {username, password}, {username, password: newPassword})
        .then((writeObj) => {
          db.close();
          if (writeObj.modifiedCount === 1) {
            return `User ${username} updated`;
          }

          throw `not found`;
        })
    );

//find user, then delete if their pw matches
const deleteUser = (username, password) =>
  MongoClient.connect(url)
    .then((db) => removeUser(db, {username, password})
      .then((writeObj) => {
        db.close();

        if (writeObj.result.n !== 0) {
          return `List ${username} deleted`;
        }

        throw `list '${username}' not found`;
      })
    );

//true param removes one list
const removeUser = (db, filters) =>
  db.collection(`${AUTH_COLLECTION_NAME}`)
  .remove(filters, true);

const replaceUser = (db, filters, replacement) =>
  db.collection(`${AUTH_COLLECTION_NAME}`)
    .replaceOne(filters, replacement);

const addOneUser = (db, userObj) =>
  db.collection(`${AUTH_COLLECTION_NAME}`)
    .insertOne(userObj);

const findUser = (db, filters) =>
  db.collection(`${AUTH_COLLECTION_NAME}`)
    .findOne(filters, {_id: 0});

module.exports = {
  addUser,
  updateUser,
  checkUser,
  checkUserExists,
  //unused
  deleteUser,
};