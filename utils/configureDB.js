'use strict';
/*eslint-env node, es6*/
require('dotenv').config();

const MongoClient = require('mongodb').MongoClient;
const AUTH_COLLECTION_NAME = 'AUTH';

const testUsers = require('./testUsers.js');

module.exports = exports = (MONGO_URL) => {

  const dropCollectionIfExists = (db, collectionName) => new Promise((resolve) => {
    db.collection(collectionName, {strict: true}, (err, collection) => {
      if (err) {
        console.log(`Cannot drop collection '${collectionName}': ${err}`);
        resolve();
        return;
      }

      collection.drop()
      console.log(`Dropped collection '${collectionName}'`);
      resolve();
    });
  });

  const addConstraints = (db) => new Promise((resolve, reject) => {
    db.collection(AUTH_COLLECTION_NAME)
      .createIndex({username: 1}, {unique:true})
      .then(() => {
        console.log('username unique index added');
        resolve();
      }).catch((err) => {
        reject(err);
      });
  });

  const clean = () => new Promise((resolve, reject) => {
    MongoClient.connect(MONGO_URL)
      .then((db) => dropCollectionIfExists(db, AUTH_COLLECTION_NAME)
          .then(() => addConstraints(db))
          .then(() => {
            db.close();
            resolve();
          })
          .catch((errr) => {
            db.close();
            console.log(`clean error: ${errr}`);
            reject();
          })
      ).catch((err) => {
        reject(err);
      });
  });

  //right now load does nothigng for the auth DB
  const load = () => {
    return MongoClient.connect(MONGO_URL)
      .then((db) => {
        db.collection(AUTH_COLLECTION_NAME)
          .insert(testUsers)
          .then((writeObj) => {
            console.log('test users loaded');
            console.log(writeObj);
          })
          .catch((err) => {
            console.log('loading failed');
            console.log(err);
          })
          .then(() => {
            db.close();
          });
      })
      .catch((err) => {
        console.log('load connection error');
        console.log(err);
      })
  };

  return {
    load,
    clean,
  };
};