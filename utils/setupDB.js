'use strict';
/*eslint-env node, es6*/

//commandline variation of sendCardsToMongo
//sendCards folderPathToCardData mongoUrl
//eg node setupDB.js clean

const clearDB = (process.argv[2] === 'clean');
const dbWrapper = require('./configureDB.js')(process.env.MONGODB_URI)

if (clearDB) {
  console.log('cleaning DB');
  dbWrapper.clean()
    .catch((err) => console.log(err));
}
console.log('loading DB');
dbWrapper.load();
